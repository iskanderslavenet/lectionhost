const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

var mongoose = require('mongoose');
var authoIncrement = require('mongoose-auto-increment');

var express = require('express');
var app = express();

var videos = require('./models/preview');
var lections = require('./models/lection');
var command = require('./models/command');

// init static resources
app.use( express.static( `${__dirname}/public`));

// init mongoDB connection and schemas
mongoose.connect("mongodb://localhost:27017/local", { useNewUrlParser: true });
var connection = mongoose.connection;

authoIncrement.initialize(connection);
videos.plugin(authoIncrement.plugin, {model: 'videos', field: 'videoId'});
videos.plugin(authoIncrement.plugin, {model: 'video_test', field: 'videoId'});
command.plugin(authoIncrement.plugin, {model: 'command', field: 'id'});
lections.plugin(authoIncrement.plugin, {model: 'lections', field: 'lectionId'});

var Videos = mongoose.model("videos", videos);
var Lections = mongoose.model("lections", lections);
var Command = mongoose.model("command", command);

// set up render engine
app.set("view engine", "ejs");

app.get('/', (req, res) => {
    res.redirect("/videos");
})

app.get('/command', (req, res) => {
    MongoClient.connect(
        'mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass%20Community&ssl=false',
        { useNewUrlParser: true, useUnifiedTopology: true },
        function(connectErr, client) {
          assert.equal(null, connectErr);
          const coll = client.db('local').collection('command');
          coll.find().toArray((error, result) => {
              var retObject = { command: result };
              res.render("command", retObject);
              console.log(result)
          });
          client.close();
        });

    /*var previewModel = Command.find({id: 0}, (err, doc) => {
        console.log(doc);
        var retObject = { command: doc };
        res.render("command", retObject);
    });*/
});

app.get('/about', (req, res) => {
    res.render('about', {});
});

app.get('/videos', (req, res) => {
    var previewModel = Videos.find({}, (err, doc) => {
        console.log(doc);
        var retObject = { videos: doc };
        res.render("videos", retObject);
    });
});

app.get('/videos/:id', (req, res) => {
    var param = req.params.id;
    var previewModel = Videos.findOne({videoId: param}, (err, doc) => {
        console.log(doc);
        res.render("video", doc);
    });
});

app.get('/lections', (req, res) => {
    var previewModel = Lections.find({}, (err, doc) => {
        console.log(doc);
        var retObject = { lections: doc };
        res.render("lections", retObject);
    });
});

app.get('/lections/:id', (req, res) => {
    var param = req.params.id;
    var previewModel = Lections.findOne({lectionId: param}, (err, doc) => {
        console.log(doc);
        res.render("lection", doc);
    });
});

// set up headers for CORS-policy
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.listen(3000);
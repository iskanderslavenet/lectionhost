var mongoose = require('mongoose');

const preview = new mongoose.Schema({
    name: String,
    preview: String,
    lectionText: String,
    lectionId: Number
});

module.exports = preview;
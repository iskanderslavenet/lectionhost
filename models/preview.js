var mongoose = require('mongoose');

const preview = new mongoose.Schema({
    name: String,
    description: String,
    videoId: Number
});

module.exports = preview;
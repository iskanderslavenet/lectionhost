var mongoose = require('mongoose');

const preview = new mongoose.Schema({
    id: Number,
    name: String,
    role: String
});

module.exports = preview;